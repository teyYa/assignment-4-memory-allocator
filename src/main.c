#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#define MAP_FLAGS 0x20
#define HEAP_INIT_SIZE 1024
#define FIRST_MEM_SIZE 512
#define SECOND_MEM_SIZE 256
#define THIRD_MEM_SIZE 128
#define FOURTH_MEM_SIZE 2048

static void allocation_test(char* test_name, size_t heap_init_size, size_t first_mem, size_t second_mem, bool split_heap) {
    printf("--- Starting test %s ---\n", test_name);
    void* heap = heap_init(heap_init_size);
    assert(heap);
    debug_heap(stdout, heap);

    //making initialized memory somewhere after heap
    if(split_heap) assert(mmap( heap + heap_init_size*2, 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FLAGS, -1, 0 ) != MAP_FAILED);

    void* mem1 = _malloc(first_mem);
    assert(mem1);
    debug_heap(stdout, heap);
    printf("first memory allocated\n");

    if(second_mem) {
        void* mem2 = _malloc(second_mem);
        assert(mem2);
        debug_heap(stdout, heap);
        printf("second memory allocated\n");

        _free(mem2);
    }

    _free(mem1);
    heap_term();
    printf("all memory and heap freed\n"); 
    printf("--- Test %s ended successfully ---\n\n", test_name);
}

int main() {

    allocation_test("simple memory allocation and free", HEAP_INIT_SIZE, FIRST_MEM_SIZE, 0, false);
    allocation_test("multiple memory allocation and free", HEAP_INIT_SIZE, SECOND_MEM_SIZE, SECOND_MEM_SIZE, false);
    allocation_test("heap growing", SECOND_MEM_SIZE, THIRD_MEM_SIZE, HEAP_INIT_SIZE, false);
    allocation_test("heap growing in new place", SECOND_MEM_SIZE, THIRD_MEM_SIZE, FOURTH_MEM_SIZE, true);
    
    return 0;
}
