#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  /*  !!! */  
  block_size requested_size = size_from_capacity((block_capacity) {query});
  size_t actual_size = region_actual_size(requested_size.bytes);

  //try map at exact address
  void* memory_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);
  //try map somewhere
  if(memory_addr == MAP_FAILED) memory_addr = map_pages(addr, actual_size, 0);
  if(memory_addr == MAP_FAILED) return REGION_INVALID;

  block_init(memory_addr, (block_size) {.bytes = actual_size}, NULL);

  return (struct region) {
    .addr = memory_addr,
    .size = actual_size,
    .extends = memory_addr == addr
  };
}

static void* block_after( struct block_header const* block )         ;
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header* first_block = (struct block_header*) HEAP_START;
  struct block_header* to_free = first_block;
  struct block_header* next_block;

  size_t to_unmap = 0;

  //iterate blocks to the end
  while(to_free) {
    //accumulate size to unmap
    to_unmap += size_from_capacity(to_free->capacity).bytes;
    next_block = to_free->next;

    //if next block isnt close to current - unmap part from first block
    if(!blocks_continuous(to_free, next_block)) {
      munmap(first_block, to_unmap);
      first_block = next_block;
      to_unmap = 0;
    }
    to_free = next_block;
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  //prevent too small first block
  query = size_max(query, BLOCK_MIN_CAPACITY);
  //prevent too small second block
  if(!block_splittable(block, query)) return false;

  size_t second_size = block->capacity.bytes - query;
  struct block_header* second = (struct block_header*) (block->contents + query);
  block_init(second, (block_size) {second_size}, block->next);

  block->capacity = (block_capacity) {query};
  block->next = second;

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  struct block_header* next_block = block->next;
  if(!next_block || !mergeable(block, next_block)) return false;

  //increase cap for size of block
  block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
  block->next = next_block->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

  while (block) {
    //if block is free
    if(block->is_free){
      //and merging if it still isnt big enough
      while (try_merge_with_next(block));
      //trying to return it
      if (block_is_big_enough(sz, block)) return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
    } 

    if(!block->next) return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
    block = block->next;
  }

  return (struct block_search_result) {BSR_CORRUPTED, NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result res = find_good_or_last(block, query);

  if (res.type == BSR_FOUND_GOOD_BLOCK) {
      split_if_too_big(res.block, query);
      res.block->is_free = false;
  }
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct region reg = alloc_region(block_after(last), query);
  if(region_is_invalid(&reg)) return NULL;

  block_init(reg.addr, (block_size) {reg.size}, NULL);
  last->next = reg.addr;
  if (reg.extends && try_merge_with_next(last)) return last;
  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result res = try_memalloc_existing(query, heap_start);
  if (res.type == BSR_REACHED_END_NOT_FOUND) {
    struct block_header* new_block = grow_heap(res.block, query);
    if(new_block) res = try_memalloc_existing(query, new_block);
  }

  if (res.type == BSR_FOUND_GOOD_BLOCK) return res.block;
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
